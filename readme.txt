#
# About
#

Library to read well-formed XML via a streaming API. 

Built very similarly to Qt's QXmlStreamReader, which documention
can be found here: https://doc.qt.io/qt-5/qxmlstreamreader.html

I couldn't find a good stream-based XML parser library for fortran.
There are lots of  XML libraries, but most of them are quite big and
uses either DOM or SAX parsing of XML files, while I think stream  
based parsing is both faster and simpler to use.

See example.f90 for an example of reading a moderately complicated xml
file into a datastructure.

Currently this parser does not support advanced stuff in XML files,
like namespacing or parsing processing instructions.

#
# Requires
#

Fortran 2003 is required to build the library.
