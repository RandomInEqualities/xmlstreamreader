module xs_streamlinetracker_module
  
use xs_characters_module
use xs_stream_module
  
implicit none
private

public :: xs_StreamLineTracker

! Wrapper around a xs_Stream to track the line and column position that we 
! are currently at in the stream.
type :: xs_StreamLineTracker
  private
  integer :: line
  integer :: column
  class(xs_Stream), pointer :: stream => null()
  integer(kind=1) :: last_byte
contains
  private
  procedure, public :: set_stream
  procedure, public :: peek
  procedure, public :: get
  procedure, public :: at_last
  procedure, public :: at_error
  procedure, public :: get_error_message
  procedure, public :: current_line
  procedure, public :: current_column
end type


contains


subroutine set_stream(this, stream) 
  class(xs_StreamLineTracker), intent(inout) :: this
  class(xs_Stream), target, intent(inout) :: stream
  this%stream => stream
  this%line = 1
  this%column = 0
  this%last_byte = 0
end subroutine


function peek(this, status) result(next_byte)
  class(xs_StreamLineTracker), intent(inout) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  next_byte = this%stream%peek(status)
end function


function get(this, status) result(next_byte)
  class(xs_StreamLineTracker), intent(inout) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  next_byte = this%stream%get(status)
  if (status == xs_StreamStatusOk) then
    if (next_byte == xs_BYTE_NEW_LINE) then
      this%line = this%line + 1
      this%column = 1
    elseif ((next_byte == xs_BYTE_CARRIAGE_RETURN) .and. this%last_byte == xs_BYTE_NEW_LINE) then
      this%column = 1
    else
      this%column = this%column + 1
    endif
    this%last_byte = next_byte
  endif
end function


function at_last(this) result(is_at_last)
  class(xs_StreamLineTracker), intent(in) :: this
  logical :: is_at_last
  is_at_last = this%stream%at_last()
end function


function at_error(this) result(is_at_error)
  class(xs_StreamLineTracker), intent(in) :: this
  logical :: is_at_error
  is_at_error = this%stream%at_error()
end function


function get_error_message(this) result(error_message)
  class(xs_StreamLineTracker), intent(in) :: this
  character(len=:), allocatable ::  error_message
  
  error_message = this%stream%get_error_message()
end function


function current_line(this) result(line)
  class(xs_StreamLineTracker), intent(in) :: this
  integer :: line
  line = this%line
end function


function current_column(this) result(column)
  class(xs_StreamLineTracker), intent(in) :: this
  integer :: column
  column = this%column
end function


end module