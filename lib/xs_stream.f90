module xs_stream_module
  
implicit none
private

public :: xs_Stream, xs_StreamStatusOk, xs_StreamStatusEof, xs_StreamStatusError

enum, bind(c)
  enumerator :: xs_StreamStatusOk
  enumerator :: xs_StreamStatusEof
  enumerator :: xs_StreamStatusError
end enum

! Base class for representing a stream of bytes.
!
! Meant to be subclassed for files, bytes arrays, etc. See for example xs_FileStream. 
!
! The stream only works with bytes. Methods returns signed 8-bit integers. This will in
! most cases represent an ASCII character. It can also represent part of a UTF-8 multibyte 
! character and so on for all possible encodings.
!
! Inspired from C++ input streams (std::istream).
type, abstract :: xs_Stream
contains
  procedure (peek), deferred :: peek
  procedure (get), deferred :: get
  procedure (at_last), deferred :: at_last
  procedure (at_error), deferred :: at_error
  procedure (get_error_message), deferred :: get_error_message
end type

abstract interface

  ! Routine to look ahead in the stream.
  !
  ! Returns the ascii character that the next get() call will return.
  function peek(this, status) result(next_byte)
    import :: xs_Stream
    class(xs_Stream), intent(in) :: this
    integer, intent(out) :: status
    integer(kind=1) :: next_byte
  end function
  
  ! Routine to get the next ascii character in the stream.
  function get(this, status) result(next_byte)
    import :: xs_Stream
    class(xs_Stream), intent(inout) :: this
    integer, intent(out) :: status
    integer(kind=1) :: next_byte
  end function
  
  ! Routine to check if we are at the last character in the stream.  
  function at_last(this) result(is_at_last)
    import :: xs_Stream
    class(xs_Stream), intent(in) :: this
    logical :: is_at_last
  end function
  
  ! Routine to check if an error in the stream have occured.
  function at_error(this) result(is_at_error)
    import :: xs_Stream
    class(xs_Stream), intent(in) :: this
    logical :: is_at_error
  end function
  
  ! The message associated with an error.
  function get_error_message(this) result(error_message)
    import :: xs_Stream
    class(xs_Stream), intent(in) :: this
    character(len=:), allocatable ::  error_message
  end function
  
end interface

end module