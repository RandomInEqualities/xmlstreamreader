module xs_characters_module

implicit none

! The ascii integer value of various characters.
integer(kind=1), parameter :: xs_BYTE_TAB = 9 ! \t
integer(kind=1), parameter :: xs_BYTE_NEW_LINE = 10 ! \n
integer(kind=1), parameter :: xs_BYTE_CARRIAGE_RETURN = 13 ! \r
integer(kind=1), parameter :: xs_BYTE_SPACE = 32 ! =  
integer(kind=1), parameter :: xs_BYTE_ESCLAMATION = 33 ! !
integer(kind=1), parameter :: xs_BYTE_DOUBLE_QUOTE = 34 ! "
integer(kind=1), parameter :: xs_BYTE_AMPERSAND = 38 ! &
integer(kind=1), parameter :: xs_BYTE_QUOTE = 39 ! '
integer(kind=1), parameter :: xs_BYTE_DASH = 45 ! -
integer(kind=1), parameter :: xs_BYTE_PERIOD = 45 ! .
integer(kind=1), parameter :: xs_BYTE_SLASH_FORWARD = 47 ! /
integer(kind=1), parameter :: xs_BYTE_SEMI_COLON = 59 ! ;
integer(kind=1), parameter :: xs_BYTE_LESS_THAN = 60 ! <
integer(kind=1), parameter :: xs_BYTE_EQUAL = 61 ! =
integer(kind=1), parameter :: xs_BYTE_GREATER_THAN = 62 ! >
integer(kind=1), parameter :: xs_BYTE_QUESTION_MARK = 63 ! ?
integer(kind=1), parameter :: xs_BYTE_SLASH_BACKWARD = 92 ! \
integer(kind=1), parameter :: xs_BYTE_UNDERSCORE = 95 ! _

! The ascii integer values of xml escape sequences.
integer(kind=1), dimension(6), parameter :: xs_ESCAPE_DOUBLE_QUOTE = [38, 113, 117, 111, 116, 59] ! &quot;
integer(kind=1), dimension(6), parameter :: xs_ESCAPE_QUOTE = [38, 97, 112, 111, 115, 59] ! &apos;
integer(kind=1), dimension(5), parameter :: xs_ESCAPE_AMPERSAND = [38, 97, 109, 112, 59] ! &amp;
integer(kind=1), dimension(4), parameter :: xs_ESCAPE_LESS_THAN = [38, 108, 116, 59] ! &lt;
integer(kind=1), dimension(4), parameter :: xs_ESCAPE_GREATER_THAN = [38, 103, 116, 59] ! &gt;


contains


function xs_is_whitespace(char) result(is_whitespace)
  integer(kind=1), intent(in) :: char
  logical :: is_whitespace
  
  is_whitespace = ((char == xs_BYTE_TAB) .or. (char == xs_BYTE_NEW_LINE) .or. &
    (char == xs_BYTE_CARRIAGE_RETURN) .or. (char == xs_BYTE_SPACE))
end function


function xs_is_newline(char) result(is_newline)
  integer(kind=1), intent(in) :: char
  logical :: is_newline
  
  is_newline = ((char == xs_BYTE_NEW_LINE) .or. (char == xs_BYTE_CARRIAGE_RETURN))
end function


function xs_is_letter(char) result(is_letter)
  integer(kind=1), intent(in) :: char
  logical :: is_letter
  
  is_letter = ((char >= 65 .and. char <= 90) .or. (char >= 97 .and. char <= 122))
end function


function xs_is_numeric(char) result(is_numeric)
  integer(kind=1), intent(in) :: char
  logical :: is_numeric
  
  is_numeric = (char >= 48 .and. char <= 57)
end function


function xs_to_char(char_byte) result(char_string)
  integer(kind=1), intent(in) :: char_byte
  character(len=1) :: char_string
  
  char_string = CHAR(char_byte)
end function


function xs_to_byte(char_string) result(char_byte)
  character(len=1), intent(in) :: char_string
  integer(kind=1) :: char_byte
  
  char_byte = ICHAR(char_string, kind=1)
end function


function xs_to_chars(char_bytes) result(char_string)
  integer(kind=1), dimension(:), intent(in) :: char_bytes
  character(len=size(char_bytes)) :: char_string
  
  integer :: i
  
  do i=1,size(char_bytes)
    char_string(i:i) = xs_to_char(char_bytes(i))
  enddo
end function


function xs_is_printable(char) result(is_printable)
  integer(kind=1), intent(in) :: char
  logical :: is_printable
  
  is_printable = (char >= 33 .and. char <= 126)
end function


function xs_to_printable(char_byte) result(char_string)
  integer(kind=1), intent(in) :: char_byte
  character(len=:), allocatable :: char_string
  
  if (xs_is_printable(char_byte)) then
    char_string = xs_to_char(char_byte)
  elseif (xs_is_whitespace(char_byte)) then
    char_string = "whitespace"
  else
    char_string = "control"
  endif
end function


end module