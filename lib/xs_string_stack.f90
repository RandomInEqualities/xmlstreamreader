module xs_string_stack_module

implicit none
private

public :: xs_StringStack

type :: StringPointer
  character(len=:), pointer :: string_p => null()
end type

! A stack of variable length strings.
type :: xs_StringStack
  private
  type(StringPointer), dimension(:), allocatable :: storage
  integer :: used_space = 0
contains
  private
  procedure, public :: is_empty
  procedure, public :: length
  procedure, public :: push
  procedure, public :: pop
  procedure, public :: top
  procedure, public :: clear
  procedure :: copy
  generic, public :: assignment(=) => copy
  final :: destructor
end type


contains


function is_empty(this)
  class(xs_StringStack), intent(in) :: this
  logical :: is_empty
  
  is_empty = (this%used_space == 0)
end function


function length(this)
  class(xs_StringStack), intent(in) :: this
  integer :: length
  
  length = this%used_space
end function


! Push a string on top of the stack.
subroutine push(this, string)
  class(xs_StringStack), intent(inout) :: this
  character(len=*), intent(in) :: string
  
  integer :: max_size
  integer :: next_size
  type(StringPointer), dimension(:), allocatable :: temporary_storage
  integer :: index
  
  if (allocated(this%storage)) then
    max_size = size(this%storage)
    next_size = 2 * max_size
  else
    max_size = 0
    next_size = 10
  endif
  
  if (this%used_space == max_size) then
    allocate(temporary_storage(next_size))
    do index=1,this%used_space
      temporary_storage(index) = this%storage(index)
    enddo
    call move_alloc(temporary_storage, this%storage)
  endif
  
  this%used_space = this%used_space + 1
  allocate(character(len=len(string)) :: this%storage(this%used_space)%string_p)
  this%storage(this%used_space)%string_p = string
end subroutine


! Pop a string off the top of the stack
subroutine pop(this)
  class(xs_StringStack), intent(inout) :: this
  
  if (this%used_space > 0) then
    deallocate(this%storage(this%used_space)%string_p)
    this%storage(this%used_space)%string_p => null()
    this%used_space = this%used_space - 1
  endif
end subroutine


! Get the string at the top of the stack.
function top(this) result(string)
  class(xs_StringStack), intent(in) :: this
  character(len=:), pointer :: string

  string => null()
  if (this%used_space > 0) then
    string => this%storage(this%used_space)%string_p
  endif
end function


! Remove all strings in the stack.
subroutine clear(this)
  class(xs_StringStack), intent(inout) :: this
  
  integer :: index
  
  do index=1,this%used_space
    deallocate(this%storage(index)%string_p)
    this%storage(index)%string_p => null()
  enddo
  this%used_space = 0
end subroutine


subroutine copy(this, other)
  class(xs_StringStack), intent(out) :: this
  class(xs_StringStack), intent(in) :: other

  integer :: index
  
  call this%clear()
  do index=1,other%used_space
    call this%push(other%storage(index)%string_p)
  enddo
end subroutine


impure elemental subroutine destructor(this)
  type(xs_StringStack), intent(inout) :: this
  
  call this%clear()
end subroutine


end module