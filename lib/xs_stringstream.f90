module xs_stringstream_module

use xs_stream_module
use xs_characters_module, only: xs_to_byte

implicit none
private

public :: xs_StringStream, xs_Stream

! A stream representation of a string.
!
! See xs_Stream for more documentation.
type, extends(xs_Stream) :: xs_StringStream
  private
  integer :: position = 0
  character(len=:), allocatable :: string
contains
  private
  procedure, public :: set_string
  procedure, public :: peek
  procedure, public :: get
  procedure, public :: at_last
  procedure, public :: at_error
  procedure, public :: get_error_message
end type


contains


subroutine set_string(this, string)
  class(xs_StringStream), intent(inout) :: this
  character(len=*), intent(in) :: string
  
  this%position = 0
  this%string = trim(string)
end subroutine


function peek(this, status) result(next_byte)
  class(xs_StringStream), intent(in) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  integer :: index
  
  next_byte = 0
  if (this%at_error()) then
    status = xs_StreamStatusError
  else if (this%at_last()) then
    status = xs_StreamStatusEof
  else
    status = xs_StreamStatusOk
    index = this%position + 1
    next_byte = xs_to_byte(this%string(index:index))
  endif
end function


function get(this, status) result(next_byte)
  class(xs_StringStream), intent(inout) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  integer :: index
  
  next_byte = 0
  if (this%at_error()) then
    status = xs_StreamStatusError
  else if (this%at_last()) then
    status = xs_StreamStatusEof
  else
    status = xs_StreamStatusOk
    this%position = (this%position + 1)
    index = this%position
    next_byte = xs_to_byte(this%string(index:index))
  endif
end function


function at_last(this) result(is_at_last)
  class(xs_StringStream), intent(in) :: this
  logical :: is_at_last
  
  if (this%at_error()) then
    is_at_last = .true.
  else
    is_at_last = (this%position >= len(this%string))
  endif
end function


function at_error(this) result(is_at_error)
  class(xs_StringStream), intent(in) :: this
  logical :: is_at_error
  
  if (allocated(this%string)) then
    is_at_error = (len(this%string) == 0)
  else
    is_at_error = .true.
  endif
end function


function get_error_message(this) result(error_message)
  class(xs_StringStream), intent(in) :: this
  character(len=:), allocatable ::  error_message
  
  if (.not. this%at_error()) then
    error_message = "no error"
  elseif (allocated(this%string)) then
    error_message = "string is empty"
  else
    error_message = "string is not allocated"
  endif
end function


end module
