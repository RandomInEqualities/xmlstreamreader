module xs_bytestream_module
  
use xs_stream_module
  
implicit none
private

public :: xs_ByteStream, xs_Stream

! A stream representation of an array of bytes.
!
! See xs_Stream for more documentation.
type, extends(xs_Stream) :: xs_ByteStream
  private
  integer :: position = 0
  integer(kind=1), dimension(:), allocatable :: bytes
contains
  private
  procedure, public :: set_bytes
  procedure, public :: peek
  procedure, public :: get
  procedure, public :: at_last
  procedure, public :: at_error
  procedure, public :: get_error_message
end type


contains


subroutine set_bytes(this, bytes)
  class(xs_ByteStream), intent(inout) :: this
  integer(kind=1), dimension(:), intent(in), allocatable :: bytes
  
  this%position = 0
  this%bytes = bytes
end subroutine


function peek(this, status) result(next_byte)
  class(xs_ByteStream), intent(in) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  next_byte = 0
  if (this%at_error()) then
    status = xs_StreamStatusError
  else if (this%at_last()) then
    status = xs_StreamStatusEof
  else
    status = xs_StreamStatusOk
    next_byte = this%bytes(this%position + 1)
  endif
end function


function get(this, status) result(next_byte)
  class(xs_ByteStream), intent(inout) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  next_byte = 0
  if (this%at_error()) then
    status = xs_StreamStatusError
  else if (this%at_last()) then
    status = xs_StreamStatusEof
  else
    status = xs_StreamStatusOk
    this%position = (this%position + 1)
    next_byte = this%bytes(this%position)
  endif
end function


function at_last(this) result(is_at_last)
  class(xs_ByteStream), intent(in) :: this
  logical :: is_at_last
  
  if (this%at_error()) then
    is_at_last = .true.
  else
    is_at_last = (this%position >= size(this%bytes))
  endif
end function


function at_error(this) result(is_at_error)
  class(xs_ByteStream), intent(in) :: this
  logical :: is_at_error
  
  if (allocated(this%bytes)) then
    is_at_error = (size(this%bytes) == 0)
  else
    is_at_error = .true.
  endif
end function


function get_error_message(this) result(error_message)
  class(xs_ByteStream), intent(in) :: this
  character(len=:), allocatable ::  error_message
  
  if (.not. this%at_error()) then
    error_message = "no error"
  elseif (allocated(this%bytes)) then
    error_message = "byte array is empty"
  else
    error_message = "byte array is not allocated"
  endif
end function


end module