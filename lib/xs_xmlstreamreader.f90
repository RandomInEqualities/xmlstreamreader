module xs_xmlstreamreader_module

use xs_characters_module
use xs_stream_module
use xs_streamlinetracker_module
use xs_string_builder_module
use xs_string_stack_module

implicit none
private

public :: xs_XmlStreamReader, xs_XmlAttribute

enum, bind(c)
  enumerator :: xs_TokenStartDocument
  enumerator :: xs_TokenEndDocument
  enumerator :: xs_TokenStartElement
  enumerator :: xs_TokenEndElement
end enum

! Class to represent an xml attrribute (name-value pair)
type xs_XmlAttribute
  character(len=:), allocatable :: name
  character(len=:), allocatable :: value
end type

! Class to read well-formed XML via a streaming API. 
!
! The basic concept is to sequentially read start and end elements. A basic usage loop could be:
!
! type(xs_XmlStreamReader) :: xml
!
! do
!   call xml%read_next()
!
!   if (xml%have_error()) then
!     ! Handle the error.
!   endif
!
!   if (xml%at_end()) then
!     ! Handle the end of file.
!   endif
!   
!   if (xml%is_start_element()) then
!     ! Handle the start element.
!   else
!     ! Handle the end element.
!   endif
! enddo
!
! Inspired from QXmlStreamReader in Qt and is basically a slimmed down version of that for fortran.
!
! It supports ASCII and UTF-8 xml files. For UTF-8 xml files the element text and attribute values
! are returned as bytes strings (character(len=:)), so some of those characters may stick together 
! and represent a UTF-8 multibyte character.
type xs_XmlStreamReader
  private
  logical :: error
  character(len=:), allocatable :: error_message
  
  type(xs_StreamLineTracker) :: stream
  integer :: current_token
  type(xs_StringStack) :: element_stack
  logical :: element_is_self_closing
  
  type(xs_XmlAttribute), dimension(:), allocatable :: attributes
  integer :: attributes_size
contains
  private
  procedure, public :: set_stream
  
  procedure, public :: have_error
  procedure, public :: get_error_message
  procedure, public :: get_current_line
  procedure, public :: get_current_column
  
  procedure, public :: at_end
  procedure, public :: is_start_element
  procedure, public :: is_end_element

  procedure, public :: name
  procedure, public :: attribute_count
  procedure, public :: attribute_index
  procedure, public :: get_attribute
  procedure, public :: get_attribute_value
  
  procedure, public :: read_next
  procedure, public :: read_element_text
  procedure, public :: skip_element
  
  procedure :: read_element
  procedure :: read_end_element
  procedure :: read_processing_instruction
  procedure :: read_comment
  procedure :: read_name
  procedure :: read_attributes
  procedure :: read_attribute
  procedure :: expand_attributes_space
  procedure :: read_attribute_value
  procedure :: read_escape_sequence
  
  procedure :: skip_whitespace
  procedure :: skip_text
  procedure :: skip_until
  procedure :: peek_char
  procedure :: peek_char_or_eof
  procedure :: read_char
  procedure :: read_char_or_eof
  procedure :: skip_char
  
  procedure :: check_equal
  procedure :: check_not_eof
  procedure :: check_not_error
  procedure, nopass :: is_valid_name_start
  procedure, nopass :: is_valid_name_char
end type


contains


! Routine to set the stream to read xml file data from.
!
! The stream can have different backends, so we can read from strings, 
! files, tcp/ip, etc.
subroutine set_stream(this, stream) 
  class(xs_XmlStreamReader), intent(inout) :: this
  class(xs_Stream), target, intent(inout) :: stream
  
  if (stream%at_error()) then
    this%error = .true.
    this%error_message = "stream error: " // stream%get_error_message()
    return
  endif
  
  call this%stream%set_stream(stream)
  
  this%error = .false.
  this%error_message = " "
  this%current_token = xs_TokenStartDocument
  this%element_is_self_closing = .false.
  call this%element_stack%clear()
  this%attributes_size = 0
end subroutine


! Routine to check if there have been a parsing error.
!
! This should be checked after every call to a method in XmlStreamReader.
function have_error(this) result(error)
  class(xs_XmlStreamReader), intent(in) :: this
  logical :: error
  
  error = this%error
end function


! Routine to get the error message for an associated error.
!
! Returns nothing if there is no error.
function get_error_message(this)  result(message)
  class(xs_XmlStreamReader), intent(in) :: this
  character(len=:), allocatable :: message
  
  if (allocated(this%error_message)) then
    message = this%error_message
  else
    message = " "
  endif
end function


! Routine to get the current line that the xml parser is at.
!
! Very useful when an error occur.
function get_current_line(this)  result(line)
  class(xs_XmlStreamReader), intent(in) :: this
  integer :: line
  
  line = this%stream%current_line()
end function


! Routine to get the current column that the xml parser is at.
!
! Very useful when an error occur.
function get_current_column(this)  result(column)
  class(xs_XmlStreamReader), intent(in) :: this
  integer :: column
  
  column = this%stream%current_column()
end function


! Routine to get the name of the currently read start or end element.
!
! If a start or end element is not read it returns an empty string.
function name(this) result(element_name)
  class(xs_XmlStreamReader), intent(in) :: this
  character(len=:), allocatable :: element_name
  
  if (this%element_stack%is_empty()) then
    element_name = " "
  else
    element_name = this%element_stack%top()
  endif
end function


! Routine to check if no more xml data can be read.
!
! This will return true when all data in the xml file have been consumed or
! if an error have occured.
function at_end(this) result(is_end)
  class(xs_XmlStreamReader), intent(in) :: this
  logical :: is_end
  
  is_end = this%have_error() .or. (this%current_token == xs_TokenEndDocument)
end function


! Routine to check if the read element is a start element.
!
! If this is true, one can fetch the element name with name() and the attrributes of the
! start element with the attribute methods.
function is_start_element(this) result(is_start)
  class(xs_XmlStreamReader), intent(in) :: this
  logical :: is_start
  
  is_start = (.not. this%have_error()) .and. (this%current_token == xs_TokenStartElement)
end function


! Routine to check if the read element is an end element.
!
! If this is true, one can fetch the element name with name().
function is_end_element(this) result(is_end)
  class(xs_XmlStreamReader), intent(in) :: this
  logical :: is_end
  
  is_end = (.not. this%have_error()) .and. (this%current_token == xs_TokenEndElement)
end function


! Routine to get the amount of attributes in a start element.
!
! This may only be called if is_start_element() is true.
function attribute_count(this) result(count)
  class(xs_XmlStreamReader), intent(in) :: this
  integer :: count
  
  count = this%attributes_size
end function


! Routine to find the index of an attribute.
!
! Returns -1 if the attribute is not present on the current start element.
function attribute_index(this, name) result(index)
  class(xs_XmlStreamReader), intent(in) :: this
  character(len=*), intent(in) :: name
  integer :: index
  
  do index=1,this%attributes_size
    if (this%attributes(index)%name == name) then
      return
    endif
  enddo
  index = -1
end function


! Routine to get an attribute for a start element.
!
! May only be called if is_start_element() is true and count is between 0 and attributes_count()
function get_attribute(this, index) result(attribute)
  class(xs_XmlStreamReader), intent(in) :: this
  integer, intent(in) :: index
  type(xs_XmlAttribute) :: attribute
  
  attribute = this%attributes(index)
end function


! Routine to get the value of an attribute for a start element.
!
! Returns an empty string if the attribute is not present in the start element.
function get_attribute_value(this, name) result(value)
  class(xs_XmlStreamReader), intent(in) :: this
  character(len=*), intent(in) :: name
  character(len=:), allocatable :: value
  
  integer :: index
  
  index = this%attribute_index(name)
  if (index /= -1) then
    value = this%attributes(index)%value
  else
    value = " "
  endif
end function


! Routine to read the next start or end element in the xml file.
!
! After calling this one can check have_error() for errors, at_end() for end of
! file data, is_start_element() for a start element and is_end_element() for an 
! end element.
subroutine read_next(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  integer(kind=1) :: next
  logical :: is_end_of_file
  
  this%attributes_size = 0
  
  if (this%current_token == xs_TokenEndDocument) then
    this%error = .true.
    this%error_message = "cannot read past end document"
    return
  endif
  
  if (this%element_is_self_closing) then
    this%element_is_self_closing = .false.
    this%current_token = xs_TokenEndElement
    return
  endif
  
  if (this%current_token == xs_TokenEndElement) then
    ! Prepare for a new element.
    call this%element_stack%pop()
  endif
  
  do
    if (this%current_token == xs_TokenStartDocument) then
      call this%skip_whitespace()
      if (this%error) return
    else
      call this%skip_text()
      if (this%error) return
    endif
    
    next = this%read_char_or_eof(is_end_of_file)
    if (this%error) return
    
    if (is_end_of_file) then
      if (this%element_stack%is_empty()) then
        this%current_token = xs_TokenEndDocument
      else
        this%error = .true.
        this%error_message = "unexpected end of file"
      endif
      return
    endif
    
    call this%check_equal(xs_BYTE_LESS_THAN, next)
    if (this%error) return
    
    next = this%peek_char()
    if (this%error) return
    
    if (is_valid_name_start(next)) then
      call this%read_element()
      return
    elseif (next == xs_BYTE_SLASH_FORWARD) then
      call this%read_end_element()
      return
    elseif (next == xs_BYTE_QUESTION_MARK) then
      call this%read_processing_instruction()
      if (this%error) return
    elseif (next == xs_BYTE_ESCLAMATION) then
      call this%read_comment()
      if (this%error) return
    else
      this%error = .true.
      this%error_message = "expected start or end element, got '" // xs_to_printable(next) // "'"
      return
    endif
  enddo
end subroutine


! Routine to read the text between a start and end element.
!
! May only be called when the current element is a start element. If a child element is
! encountered between the start and end element, an error occurs and no text is read.
function read_element_text(this) result(text)
  class(xs_XmlStreamReader), intent(inout) :: this
  character(len=:), allocatable :: text
  
  type(xs_StringBuilder) :: builder
  integer(kind=1) :: next
  
  text = " "
  
  if (this%current_token /= xs_TokenStartElement) then
    this%error = .true.
    this%error_message = "it is only possible to read text from a start element"
    return
  endif
  
  if (this%element_is_self_closing) then
    return
  endif
  
  do
    next = this%peek_char()
    if (this%error) return
    
    if (next == xs_BYTE_AMPERSAND) then
      ! Escape sequence.
      call builder%add_multiple(this%read_escape_sequence())
      if (this%error) return
    elseif (next == xs_BYTE_LESS_THAN) then
      ! End of text.
      exit
    else
      ! Include character in text.
      call builder%add(this%read_char())
      if (this%error) return
    endif
  enddo
  
  text = builder%get()
end function


! Routine to skip an element. 
!
! May only be called when the current element is a start element. Recursively skips
! all children of the current element, until the corresponding end element is encounted.
! After this the current element is the corresponding end element.
subroutine skip_element(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  integer :: level
  
  if (this%current_token /= xs_TokenStartElement) then
    this%error = .true.
    this%error_message = "it is only possible to skip a start element"
    return
  endif
  
  level = 1
  do
    call this%read_next()
    if (this%have_error()) return
    
    if (this%is_start_element()) then
      level = level + 1
    else
      level = level - 1
    endif
    
    if (level == 0) exit
  enddo
end subroutine


subroutine read_processing_instruction(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  call this%skip_char(xs_BYTE_QUESTION_MARK)
  if (this%error) return
  
  call this%skip_until((/ xs_BYTE_QUESTION_MARK, xs_BYTE_GREATER_THAN /))
  if (this%error) return
end subroutine


subroutine read_comment(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  call this%skip_char(xs_BYTE_ESCLAMATION)
  if (this%error) return
  
  call this%skip_char(xs_BYTE_DASH)
  if (this%error) return
  
  call this%skip_char(xs_BYTE_DASH)
  if (this%error) return
  
  call this%skip_until((/ xs_BYTE_DASH, xs_BYTE_DASH, xs_BYTE_GREATER_THAN /))
  if (this%error) return
end subroutine


subroutine read_element(this) 
  class(xs_XmlStreamReader), intent(inout) :: this
  
  character(len=:), allocatable :: element_name
  integer(kind=1) :: next
  
  if ((this%current_token /= xs_TokenStartDocument) .and. this%element_stack%is_empty()) then
    this%error = .true.
    this%error_message = "second root element detected"
    return
  endif
  
  this%current_token = xs_TokenStartElement
  
  element_name = this%read_name()
  if (this%error) return
  call this%element_stack%push(element_name)
  
  call this%read_attributes()
  if (this%error) return
  
  next = this%read_char()
  if (this%error) return
  
  if (next == xs_BYTE_SLASH_FORWARD) then
    this%element_is_self_closing = .true.
    call this%skip_char(xs_BYTE_GREATER_THAN)
    if (this%error) return
  elseif (next /= xs_BYTE_GREATER_THAN) then
    this%error = .true.
    this%error_message = "unexpected symbol '" // xs_to_printable(next) // "'"
  endif
end subroutine


subroutine read_end_element(this) 
  class(xs_XmlStreamReader), intent(inout) :: this
  
  character(len=:), allocatable :: element_name
  
  this%current_token = xs_TokenEndElement
  
  call this%skip_char(xs_BYTE_SLASH_FORWARD)
  if (this%error) return
  
  element_name = this%read_name()
  if (this%error) return
  
  if (this%element_stack%is_empty()) then
    this%error = .true.
    this%error_message = "got unexpected end element '" // element_name // "'"
    return
  endif
  
  if (this%element_stack%top() /= element_name) then
    this%error = .true.
    this%error_message = "got unexpected end element '" // element_name // "'"
    return
  endif
  
  call this%skip_whitespace()
  if (this%error) return
  
  call this%skip_char(xs_BYTE_GREATER_THAN)
  if (this%error) return
end subroutine


function read_name(this) result(name)
  class(xs_XmlStreamReader), intent(inout) :: this
  character(len=:), allocatable :: name

  type(xs_StringBuilder) :: builder
  integer(kind=1) :: next
  
  name = " "
  
  next = this%read_char()
  if (this%error) return
  
  if (this%is_valid_name_start(next)) then
    call builder%add(next)
  else
    this%error = .true.
    this%error_message = "element name must start with a letter or underscore"
    return
  endif
  
  do
    next = this%peek_char()
    if (this%error) return
    
    if (this%is_valid_name_char(next)) then
      call builder%add(this%read_char())
      if (this%error) return
    else
      exit
    endif
  enddo
  
  name = builder%get()
end function


subroutine read_attributes(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  integer(kind=1) :: next
  
  do
    next = this%peek_char()
    if (this%error) return
    
    if (xs_is_whitespace(next)) then
      call this%skip_whitespace()
      if (this%error) return
      
      next = this%peek_char()
      if (this%error) return
      
      if (is_valid_name_start(next)) then
        call this%read_attribute()
        if (this%error) return
      else
        return
      endif
    else  
      return
    endif
  enddo
end subroutine


subroutine read_attribute(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  type(xs_XmlAttribute) :: attribute
  
  attribute%name = this%read_name()
  if (this%error) return
  
  call this%skip_char(xs_BYTE_EQUAL)
  if (this%error) return
  
  attribute%value = this%read_attribute_value()
  if (this%error) return

  call this%expand_attributes_space()
  this%attributes_size = this%attributes_size + 1
  this%attributes(this%attributes_size) = attribute
end subroutine


function read_attribute_value(this) result(value)
  class(xs_XmlStreamReader), intent(inout) :: this
  character(len=:), allocatable :: value
  
  type(xs_StringBuilder) :: builder
  integer(kind=1) :: initial
  integer(kind=1) :: next
  
  value = " "
  
  initial = this%read_char()
  if (this%error) return
  
  if ((initial /= xs_BYTE_QUOTE) .and. (initial /= xs_BYTE_DOUBLE_QUOTE)) then
    this%error = .true.
    this%error_message = "attribute value must be surrounded by quotes"
    return
  endif
  
  do
    next = this%peek_char()
    if (this%error) return
    
    if (next == xs_BYTE_AMPERSAND) then
      call builder%add_multiple(this%read_escape_sequence())
      if (this%error) return
    else
      next = this%read_char()
      if (this%error) return
      if (next == initial) exit
      call builder%add(next)
    endif
  enddo
  
  value = builder%get()
end function


subroutine expand_attributes_space(this)
  class(xs_XmlStreamReader), intent(inout) :: this

  type(xs_XmlAttribute), dimension(:), allocatable :: new_attributes
  integer :: index
  
  if (.not. allocated(this%attributes)) then
    allocate(this%attributes(20))
    return
  endif
  
  if (this%attributes_size >= size(this%attributes)) then
    allocate(new_attributes(2 * size(this%attributes)))
    do index=1,this%attributes_size
      new_attributes(index) = this%attributes(index)
    enddo
    deallocate(this%attributes)
    this%attributes = new_attributes
  endif
end subroutine


function read_escape_sequence(this) result(characters)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1), dimension(:), allocatable :: characters
  
  integer(kind=1) :: next
  integer(kind=1), dimension(6) :: buffer
  integer :: used_space
  
  buffer(:) = 0
  characters = buffer
  
  call this%skip_char(xs_BYTE_AMPERSAND)
  if (this%error) return
  buffer(1) = xs_BYTE_AMPERSAND
  used_space = 1
  
  do
    ! Check if the escape sequence is ending prematurely.
    next = this%peek_char()
    if (this%error) return
    if ((next /= xs_BYTE_SEMI_COLON) .and. (.not. xs_is_letter(next))) exit
    
    ! Read the next character.
    next = this%read_char()
    if (this%error) return
    buffer(used_space + 1) = next
    used_space = used_space + 1
    
    ! Check if we have a match with an escape sequence.
    if (next == xs_BYTE_SEMI_COLON) then
      if (all(buffer(1:6) == xs_ESCAPE_DOUBLE_QUOTE)) then
        buffer(1) = xs_BYTE_DOUBLE_QUOTE
        used_space = 1
      elseif (all(buffer(1:6) == xs_ESCAPE_QUOTE)) then
        buffer(1) = xs_BYTE_QUOTE
        used_space = 1
      elseif (all(buffer(1:5) == xs_ESCAPE_AMPERSAND)) then
        buffer(1) = xs_BYTE_AMPERSAND
        used_space = 1
      elseif (all(buffer(1:4) == xs_ESCAPE_LESS_THAN)) then
        buffer(1) = xs_BYTE_LESS_THAN
        used_space = 1
      elseif (all(buffer(1:4) == xs_ESCAPE_GREATER_THAN)) then
        buffer(1) = xs_BYTE_GREATER_THAN
        used_space = 1
      endif
      exit
    endif
    
    if (used_space == 6) exit
  enddo
  
  characters = buffer(1:used_space)
end function


subroutine skip_whitespace(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  integer(kind=1) :: next
  logical :: is_end_of_file
  
  do
    next = this%peek_char_or_eof(is_end_of_file)
    if (this%error) return
    if (is_end_of_file) return
    if (.not. xs_is_whitespace(next)) return
    
    next = this%read_char()
    if (this%error) return
  enddo
end subroutine


subroutine skip_text(this)
  class(xs_XmlStreamReader), intent(inout) :: this
  
  integer(kind=1) :: next
  logical :: is_end_of_file
  
  do
    next = this%peek_char_or_eof(is_end_of_file)
    if (this%error) return
    if (is_end_of_file) return
    if (next == xs_BYTE_LESS_THAN) return
    
    next = this%read_char()
    if (this%error) return
  enddo
end subroutine


subroutine skip_until(this, until)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1), dimension(:), intent(in) :: until
  
  integer(kind=1), dimension(size(until)) :: current
  integer(kind=1) :: next
  logical :: is_equal
  integer :: index
  
  ! Fill current with values.
  do index=1,size(until)
    next = this%read_char()
    if (this%error) return
    current(index) = next
  enddo
  
  do 
    ! Do we have the decired sequence?
    is_equal = .true.
    do index=1,size(until)
      if (current(index) /= until(index)) then
        is_equal = .false.
        exit
      endif
    enddo
    if (is_equal) return
    
    ! Fetch next value.
    next = this%read_char()
    if (this%error) return
    
    ! Add next to the back of current.
    do index=2,size(current)
      current(index-1) = current(index)
    enddo
    current(size(current)) = next
  enddo
end subroutine


function peek_char(this) result(char)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1) :: char
  integer :: status
  
  char = this%stream%peek(status)
  call this%check_not_error(status)
  if (this%error) return
  call this%check_not_eof(status)
  if (this%error) return
end function


function peek_char_or_eof(this, is_end_of_file) result(char)
  class(xs_XmlStreamReader), intent(inout) :: this
  logical, intent(out) :: is_end_of_file
  integer(kind=1) :: char
  
  integer :: status
  
  char = this%stream%peek(status)
  call this%check_not_error(status)
  if (this%error) return
  is_end_of_file = (status == xs_StreamStatusEof)
end function


function read_char(this) result(char)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1) :: char
  
  integer :: status
  
  char = this%stream%get(status)
  call this%check_not_error(status)
  if (this%error) return
  call this%check_not_eof(status)
  if (this%error) return
end function


function read_char_or_eof(this, is_end_of_file) result(char)
  class(xs_XmlStreamReader), intent(inout) :: this
  logical, intent(out) :: is_end_of_file
  integer(kind=1) :: char
  
  integer :: status
  
  char = this%stream%get(status)
  call this%check_not_error(status)
  if (this%error) return
  is_end_of_file = (status == xs_StreamStatusEof)
end function


subroutine skip_char(this, char)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1), intent(in) :: char
  
  integer(kind=1) :: next
  integer :: status
  
  next = this%stream%get(status)
  call this%check_not_error(status)
  if (this%error) return
  call this%check_not_eof(status)
  if (this%error) return
  call this%check_equal(char, next)
  if (this%error) return
end subroutine


subroutine check_not_error(this, status)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer, intent(in) :: status
  
  if (status == xs_StreamStatusError) then
    this%error = .true.
    this%error_message = "stream error: " // this%stream%get_error_message()
  endif
end subroutine


subroutine check_not_eof(this, status)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer, intent(in) :: status
  
  if (status == xs_StreamStatusEof) then
    this%error = .true.
    this%error_message = "unexpected end of file"
  endif
end subroutine


subroutine check_equal(this, expected, got)
  class(xs_XmlStreamReader), intent(inout) :: this
  integer(kind=1), intent(in) :: expected
  integer(kind=1), intent(in) :: got
  
  if (expected /= got) then
    this%error = .true.
    this%error_message = "expected '" // xs_to_printable(expected) // "', got '" // xs_to_printable(got) // "'"
  endif
end subroutine


function is_valid_name_start(char) result(is_valid)
  integer(kind=1), intent(in) :: char
  logical :: is_valid
  
  is_valid = xs_is_letter(char) .or. xs_is_numeric(char) .or. (char == xs_BYTE_UNDERSCORE)
end function


function is_valid_name_char(char) result(is_valid)
  integer(kind=1), intent(in) :: char
  logical :: is_valid
  
  is_valid = xs_is_letter(char) .or. xs_is_numeric(char) .or. (char == xs_BYTE_UNDERSCORE) &
    .or. (char == xs_BYTE_PERIOD) .or. (char == xs_BYTE_DASH)
end function


end module
