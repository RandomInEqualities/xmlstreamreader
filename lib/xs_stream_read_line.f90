module xs_stream_read_line_module
  
use xs_stream_module
use xs_characters_module, only: xs_is_newline, xs_to_char

implicit none
private

public :: xs_read_line


contains


! Skip all leading newlines from the stream.
subroutine skip_newlines(stream)
  class(xs_Stream), intent(inout) :: stream
  
  integer(kind=1) :: next
  integer :: status
  
  do
    next = stream%peek(status)
    if (status == xs_StreamStatusError) return
    if (status == xs_StreamStatusEof) return
    if (.not. xs_is_newline(next)) return
    
    next = stream%get(status)
    if (status == xs_StreamStatusError) return
    if (status == xs_StreamStatusEof) return
  enddo
end subroutine


! Read a line from the given stream. 
!
! If a line does not have any chracters, an error occur, or end of file occur the returned
! line is empty. It is possible to check for errors and end of line, by calling stream%at_error,
! or stream%at_last after using this function.
subroutine xs_read_line(stream, line)
  class(xs_Stream), intent(inout) :: stream
  character(len=:), allocatable, intent(out) :: line
  
  integer(kind=1) :: next
  integer :: status
  
  line = ""
  do
    next = stream%peek(status)
    if (status == xs_StreamStatusEof) return
    if (status == xs_StreamStatusError) return
    
    if (xs_is_newline(next)) then
      call skip_newlines(stream)
      return
    else
      line = line // xs_to_char(stream%get(status))
      if (status == xs_StreamStatusEof) return
      if (status == xs_StreamStatusError) return
    endif
  enddo
end subroutine


end module