module xs_string_builder_module
  
use xs_characters_module
  
implicit none
private

public :: xs_StringBuilder

! A short wrapper around an array of character integers.
!
! A bit more efficient and easier to concatenate a lot of character.
type :: xs_StringBuilder
  private
  integer(kind=1), dimension(:), allocatable :: storage
  integer :: used_space = 0
contains
  private
  procedure, public :: get
  procedure, public :: add
  procedure, public :: add_multiple
end type


contains


function get(this) result(string)
  class(xs_StringBuilder), intent(inout) :: this
  character(len=:), allocatable :: string
  
  if (this%used_space == 0) then
    string = " "
  else
    string = xs_to_chars(this%storage(1:this%used_space))
  endif
end function


subroutine add(this, char)
  class(xs_StringBuilder), intent(inout) :: this
  integer(kind=1), intent(in) :: char
  
  integer :: max_size
  integer :: next_size
  integer(kind=1), dimension(:), allocatable :: temporary_storage
  integer :: index
  
  if (allocated(this%storage)) then
    max_size = size(this%storage)
    next_size = 2 * max_size
  else
    max_size = 0
    next_size = 64
  endif
  
  if (this%used_space == max_size) then
    allocate(temporary_storage(next_size))
    do index=1,this%used_space
      temporary_storage(index) = this%storage(index)
    enddo
    call move_alloc(temporary_storage, this%storage)
  endif
  
  this%used_space = this%used_space + 1
  this%storage(this%used_space) = char
end subroutine


subroutine add_multiple(this, chars) 
  class(xs_StringBuilder), intent(inout) :: this
  integer(kind=1), dimension(:), intent(in) :: chars
  
  integer :: index
  
  do index=1,size(chars)
    call this%add(chars(index))
  enddo
end subroutine


end module