module xs_filestream_module
  
use xs_stream_module
  
implicit none
private

public :: xs_FileStream, xs_Stream

! A stream representation of a file.
!
! See xs_Stream for more documentation.
type, extends(xs_Stream) :: xs_FileStream
  private
  integer :: unit
  logical :: is_unit_open = .false.
  logical :: is_at_last = .true.
  logical :: is_at_error = .false.
  character(len=:), allocatable :: error_message
  integer(kind=1) :: next_byte
contains
  private
  procedure, public :: open_file
  procedure, public :: close_file
  procedure, public :: is_file_open
  procedure, public :: peek
  procedure, public :: get
  procedure, public :: at_last
  procedure, public :: at_error
  procedure, public :: get_error_message
  procedure, private :: read_next
  procedure, private :: set_io_error
  final :: destructor
end type


contains


subroutine open_file(this, filepath)
  class(xs_FileStream), intent(inout) :: this
  character(len=*), intent(in) :: filepath
  
  integer :: error_code
  
  call this%close_file()
  
  open(newunit=this%unit, file=filepath, iostat=error_code, action="read", access="stream", form="unformatted")
  if (error_code == 0) then
    this%is_unit_open = .true.
    this%is_at_last = .false.
    this%is_at_error = .false.
    call this%read_next()
  else
    this%is_unit_open = .false.
    this%is_at_last = .true.
    this%is_at_error = .true.
    this%error_message = "could not open file '" // filepath // "'"
  endif
end subroutine


subroutine close_file(this)
  class(xs_FileStream), intent(inout) :: this
  
  if (this%is_unit_open) then
    close(this%unit)
    this%is_unit_open = .false.
  endif
end subroutine


function is_file_open(this) result(is_open)
  class(xs_FileStream), intent(in) :: this
  logical :: is_open
  
  is_open = this%is_unit_open
end function


function peek(this, status) result(next_byte)
  class(xs_FileStream), intent(in) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  next_byte = 0
  if (this%is_file_open() .and. (.not. this%is_at_error)) then
    if (this%is_at_last) then
      status = xs_StreamStatusEof
    else
      status = xs_StreamStatusOk
      next_byte = this%next_byte
    endif
  else
    status = xs_StreamStatusError
  endif
end function


function get(this, status) result(next_byte)
  class(xs_FileStream), intent(inout) :: this
  integer, intent(out) :: status
  integer(kind=1) :: next_byte
  
  next_byte = 0
  if (this%is_file_open() .and. (.not. this%is_at_error)) then
    if (this%is_at_last) then
      status = xs_StreamStatusEof
    else
      status = xs_StreamStatusOk
      next_byte = this%next_byte
      call this%read_next()
      if (this%is_at_error) status = xs_StreamStatusError
    endif
  else
    status = xs_StreamStatusError
  endif
end function


function at_last(this) result(is_at_last)
  class(xs_FileStream), intent(in) :: this
  logical :: is_at_last
  
  is_at_last = this%is_at_last
end function


function at_error(this) result(is_at_error)
  class(xs_FileStream), intent(in) :: this
  logical :: is_at_error
  
  is_at_error = this%is_at_error
end function


function get_error_message(this) result(error_message)
  class(xs_FileStream), intent(in) :: this
  character(len=:), allocatable ::  error_message
  
  error_message = this%error_message
end function


subroutine read_next(this)
  class(xs_FileStream), intent(inout) :: this
  
  integer :: error_code
  
  if (this%is_file_open()) then
    read(unit=this%unit, iostat=error_code) this%next_byte
    this%is_at_last = (error_code < 0)
    this%is_at_error = (error_code > 0)
    if (this%is_at_error) then
      call this%set_io_error(error_code)
    endif
  else
    this%is_at_error = .true.
    this%error_message = "cannot read a closed file"
  endif
end subroutine


subroutine set_io_error(this, error_code) 
  class(xs_FileStream), intent(inout) :: this
  integer, intent(in) :: error_code
  
  character(len=6) :: error_code_as_int
  
  write(error_code_as_int, *) error_code
  this%error_message = "io error: " // error_code_as_int
end subroutine


subroutine destructor(this) 
  type(xs_FileStream), intent(inout) :: this
  
  call this%close_file()
end subroutine


end module