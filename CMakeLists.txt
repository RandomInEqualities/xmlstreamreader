cmake_minimum_required(VERSION 3.5)

project(Modules Fortran)


add_library(xmlstreamreader
    lib/xs_bytestream.f90
    lib/xs_characters.f90
    lib/xs_filestream.f90
    lib/xs_stream.f90
    lib/xs_stream_read_line.f90
    lib/xs_streamlinetracker.f90
    lib/xs_string_builder.f90
    lib/xs_string_stack.f90
    lib/xs_stringstream.f90
    lib/xs_xmlstreamreader.f90
)


add_executable(example
    example.f90
)
target_link_libraries(example xmlstreamreader)
