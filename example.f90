program chracters_program
  use xs_stringstream_module
  use xs_xmlstreamreader_module
  
  implicit none
  
  character(len=:), allocatable :: xml_bytes
  type(xs_StringStream) :: xml_stream
  type(xs_XmlStreamReader) :: xml
  
  type CharacterType
    character(len=:), allocatable :: name
    character(len=:), allocatable :: mood
    character(len=:), allocatable :: line
  end type
  type(CharacterType), dimension(:), allocatable :: characters
  integer :: character_count
  integer :: character_index
  
  ! Embedded xml data, this could also come from a file.
  xml_bytes = (&
    &'&
    & <Characters>&
    &   <Character Name="Donkey Kong">&
    &     <Mood>Angry</Mood>&
    &     <Line>Hello China!</Line>&
    &   </Character>&
    &   <Character Name="Taylor Swift">&
    &     <Mood>Shouting</Mood>&
    &     <Line>Hello Europe!</Line>&
    &   </Character>&
    & </Characters>&
    &'&
  )
  
  ! Create a stream to read the xml.
  call xml_stream%set_string(xml_bytes)
  call xml%set_stream(xml_stream)
  if (xml%have_error()) then
    call print_error(xml)
    return
  endif
  
  ! Read the <Characters> element.
  call xml%read_next()
  if (xml%have_error()) then
    call print_error(xml)
    return
  endif
  if (xml%name() /= "Characters") then
    print *, "Error: could not find opening 'Characters' element"
    return
  endif
  
  ! Read the <Character> elements.
  allocate(characters(5))
  character_count = read_characters(xml, characters)
  if (xml%have_error()) then
    call print_error(xml)
    return
  endif
  if (character_count == 0) then
    print *, "Error: could not find any characters in xml"
    return
  endif
  
  ! Print the read characters.
  do character_index=1,character_count
      print *, " "
      print *, "Name: ", characters(character_index)%name
      print *, "Mood: ", characters(character_index)%mood
      print *, "Line: ", characters(character_index)%line
      print *, " "
  enddo
  
contains

  ! Read one or more <Character> elements.
  !
  ! Returns when the <Characters> element is exited or when an error occur.
  function read_characters(xml, characters) result(character_count)
    type(xs_XmlStreamReader), intent(inout) :: xml
    type(CharacterType), dimension(:), intent(inout) :: characters
    integer :: character_count
    
    character_count = 0
    
    do
      call xml%read_next()
      if (xml%have_error()) return
      if (xml%at_end()) return
      
      if (xml%is_start_element()) then
        if (xml%name() == "Character" .and. character_count < size(characters)) then
          character_count = character_count + 1
          characters(character_count) = read_character(xml)
          if (xml%have_error()) return
        else
          call xml%skip_element()
          if (xml%have_error()) return
        endif
      else
        return
      endif
    enddo
  end function

  ! Reads a <Character> xml element.
  !
  ! Returns when the <Character> element is exited or when an error occur.
  function read_character(xml) result(new_character)
    type(xs_XmlStreamReader), intent(inout) :: xml
    type(CharacterType) :: new_character
    
    new_character%name = xml%get_attribute_value("Name")
    new_character%mood = " "
    new_character%line = " "
    
    do
      call xml%read_next()
      if (xml%have_error()) return
      if (xml%at_end()) return
      
      if (xml%is_start_element()) then
        if (xml%name() == "Mood") then
          new_character%mood = xml%read_element_text()
        else if (xml%name() == "Line") then
          new_character%line = xml%read_element_text()
        endif
        if (xml%have_error()) return
        
        call xml%skip_element()
        if (xml%have_error()) return
      else
        return
      endif
    enddo
  end function
  
  ! Print any error that happened during xml parsing.
  subroutine print_error(xml)
    type(xs_XmlStreamReader), intent(in) :: xml
    
    print *, "xml error: ", xml%get_error_message()
    print *, "in line: ", xml%get_current_line()
    print *, "in column: ", xml%get_current_column()
  end subroutine

end program
